const mongoose = require("mongoose");

const Oder = mongoose.model(
  "oder",
  new mongoose.Schema({
    oderdate: {
      type: Date,
    },
    name: {
      type: String,
    },
    lastname: {
      type: String,
    },
    phonenumber: {
      type: String,
    },
    email: {
      type: String,
    },
    people: {
      type: Number,
    },
    time: {
      type: String,
    },
    zone: {
      type: String,
    },
  })
);

module.exports = Oder;
