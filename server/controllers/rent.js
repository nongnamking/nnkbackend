const mongo = require("../db/mongo")
const express = require("express")
const router = express.Router()
const Index = require("../models/oder")

router.get('/',async function (req, res){
    const index = await Index.find()
    res.send(index)

})

router.post('/',async function(req, res){
    const body = req.body;
    console.log(body)
    await Index.create(body)
    res.send("สำเร็จ")
})

router.patch('/:id',async function (req, res){
    const id = req.params.id
    const body = req.body;
    console.log(req.params)
    await Index.updateOne({
        _id : id 
    },
    {$set:body
    })
    res.send("complete")
})

router.delete('/:id',async function (req, res){
    const id = req.params.id
    await Index.deleteOne({
        _id : id
    })
    res.send("complete")
})

router.get('/:id',async function (req, res){
    const id =req.params.id
    const index = await Index.findOne({
        _id : id
    })
    res.send(index)
})

module.exports=router