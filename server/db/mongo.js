const mongoose = require("mongoose")

const uri = "mongodb+srv://man:1234@cluster0.dmuor.mongodb.net/test"
mongoose.connect(uri)

const db = mongoose.connection

db.on("error",console.error.bind(console,"connection error"))
db.once("open",()=>console.log("database ok"))

module.exports=db
