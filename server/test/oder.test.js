const app = require('../app');
const request = require("supertest")
const mongoose = require("mongoose")

describe("Add Oder", () =>{
  it("Add oder", async () => {
    const res = await ( request(app).post("/rent")).send({
      oderdate:"2021-10-06",
      name:"woraphat panklang",
      phonenumber:5444444,
      email:"wdawda@hotmail.com",
      people:3,
      time:"0205",
      zone:"เพลินใจ"
    });
    expect(res.statusCode).toEqual(200);
  });
});
afterAll(() => mongoose.disconnect());
