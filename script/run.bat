@echo off

echo "Running Script"
echo "Docker Build Server "
docker build server/. -t server

echo "Docker Run Server [port : 3000]"
docker run -d -p 3000:3000 --name server server
